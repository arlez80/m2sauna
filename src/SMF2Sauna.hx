package ;

import format.mid.Data;
import format.mid.MIDIUtil;

/**
 * SMFをaudiosaunaのxmlに変換する
 * @author あるる（きのもと 結衣）
 */
class SMF2Sauna
{
	/// 分解能
	static var TIMEBASE( default, never ) = 128;

	/**
	 * 変換
	 * @param	m
	 * @return
	 */
	static public function conv( m:MIDI ):Xml
	{
		var s2s = new SMF2Sauna( m );
		s2s.run( );
		return s2s.root;
	}

	/// 曲データ
	public var music( default, null ):MIDI;
	/// 根っこ
	public var root( default, null ):Xml;
	/// トラック数
	private var trackCount:Int;
	/// 最後の時間
	private var lastTime:Int;

	/**
	 * コンストラクタ
	 */
	public function new( music:MIDI )
	{
		this.music = music;
		this.root = Xml.createElement( "song" );
		this.lastTime = 0;

		// 初期データ（手抜き）TODO:なんとかする
		var first = "mixerVisible,true\nappArrangeToolMode,0\nappPianoRollToolMode,4\nappFocusMode,1\nappPlayHeadPosition,-11\nappPianoRollSnap,16\nappNoteWidth,25\nappNoteHeight,15\nappVOffset,478\nappHOffset,0\nappPatternWidth,4\nappPatternHeight,40\nappVOffsetArrange,0\nappHOffsetArrange,0\nappLoopStart,0\nappLoopEnd,64\nappUseLoop,true\nappCurrentEnd,64\nappFmSynthCount,4\nappVaSynthCount,2\nappSamplerCount,3\nappPatternCount,8\nappActivePatternId,1\nappActiveTrackIndex,0\nappMasterVolume,86\ndlyTime,11\ndlyDamage,0\ndlyFeed,70\ndlyLevel,100\ndlySync,true\nrvbTime,0.25\nrvbFeed,90\nrvbLevel,100\nrvbWidth,100";
		for ( line in first.split( "\n" ) ) {
			var c = line.split( "," );
			var key = c[0];
			var value = c[1];
			var child = Xml.createElement( key );
			var text = Xml.createPCData( value );
			child.addChild( text );
			this.root.addChild( child );
		}
	}

	/**
	 * 実行
	 */
	public function run( ):Void
	{
		// テンポ
		this.detectTempo( );

		// チャンネル出力
		this.root.addChild( this.generateChannels( ) );

		// トラック生成
		this.root.addChild( this.generateTracks( ) );

		// ソングパタン生成
		this.root.addChild( this.generateSongPattern( ) );

		// デバイス
		this.root.addChild( this.generateDevices( ) );

		// 最後
		var songLength = Xml.createElement( "appSongLength" );
		songLength.addChild( Xml.createPCData( Std.string( this.lastTime ) ) );
		this.root.addChild( songLength );
		var appSongEnd = Xml.createElement( "appSongEnd" );
		appSongEnd.addChild( Xml.createPCData( Std.string( this.lastTime ) ) );
		this.root.addChild( appSongEnd );
	}

	/**
	 * テンポ決定
	 */
	private function detectTempo( ):Void
	{
		var appTempo = Xml.createElement( "appTempo" );
		this.root.addChild( appTempo );

		var tempo = Xml.createPCData( "120" );
		appTempo.addChild( tempo );

		for ( track in this.music.tracks ) {
			for ( event in track.events ) {
				switch( event.type ) {
					case MIDIEventType.systemEvent( se ):
						switch( se ) {
							case MIDISystemEvent.setTempo( mpb ):
								var bpm = Math.round( 60000000 / mpb );
								tempo.nodeValue = Std.string( bpm );
							default :
								// 無視
						}
					default:
						// 無視
				}
			}
		}

		// デフォルトのまま
	}

	/**
	 * チャンネル生成
	 * @return
	 */
	private function generateChannels( ):Xml
	{
		var channels = Xml.createElement( "channels" );
		for ( id in 0 ... 10 ) {
			var channel = Xml.createElement( "channel" );
			channel.set( "channelNro", Std.string( id ) );
			channel.set( "volume", "80" );
			channel.set( "pan", "0" );
			channel.set( "delay", "0" );
			channel.set( "reverb", "8" );
			channel.set( "mute", "false" );
			channel.set( "solo", "false" );
			channel.set( "name", "MIDITrk #" + id );

			channels.addChild( channel );
		}

		return channels;
	}

	/**
	 * トラック生成
	 * @return
	 */
	private function generateTracks( ):Xml
	{
		var root = Xml.createElement( "tracks" );
		this.trackCount = 0;
		var patternId:Int = 1;

		var channels = MIDIUtil.parseChannelsFromTrack( this.music.tracks[0] );
		var channelNumber:Int = 0;

		for ( t in channels ) {
			if( 0 < t.length ) {
				var track = if ( channelNumber == 9 ) {
					this.convDrumTrack( this.trackCount, patternId, t );
				}else {
					this.convTrack( this.trackCount, patternId, t );
				}

				if ( track.elements( ).hasNext( ) ) {
					root.addChild( track );
					this.trackCount ++;
					patternId ++;
				}
			}

			channelNumber ++;
		}

		root.set( "count", Std.string( this.trackCount ) );

		return root;
	}

	/**
	 * SMFのタイムベースからaudiosaunaのタイムベースに変換
	 * @param	midiTimeBase
	 * @return
	 */
	inline private function convTimeBase( midiTimeBase:Int ):Int
	{
		return Math.round( ( midiTimeBase / this.music.timebase ) * SMF2Sauna.TIMEBASE );
	}

	/**
	 * 1つのトラックを変換（非ドラム）
	 * @param	channel
	 * @return
	 */
	private function convTrack( trackIndex:Int, patternId:Int, channel:Array<MIDIEvent> ):Xml
	{
		var root = Xml.createElement( "track" );
		root.set( "trackIndex", Std.string( trackIndex ) );
		root.set( "deviceType", "0" );		// 0:FM 1:Analog 2:Sample

		// ノート配置
		var expression:Int = 100;
		var noteCount:Int = 0;
		var notes = new Map<Int,{ start:Int, volume:Int }>( );
		for ( n in channel ) {
			switch( n.type ) {
				case MIDIEventType.noteOn( number, 0 ), MIDIEventType.noteOff( number, _ ):
					if ( notes.exists( number ) ) {
						var sn = notes.get( number );
						var endTime = this.convTimeBase( n.time );
						var note = Xml.createElement( "seqNote" );
						note.set( "startTick", Std.string( sn.start ) );
						note.set( "endTick", Std.string( endTime ) );
						note.set( "noteLength", Std.string( endTime - sn.start ) );
						note.set( "pitch", Std.string( number ) );
						note.set( "noteVolume", Std.string( sn.volume ) );
						note.set( "selected", "false" );
						note.set( "patternId", Std.string( patternId ) );
						note.set( "noteCutoff", "100" );
						root.addChild( note );
						notes.remove( number );
						if ( this.lastTime < endTime ) {
							this.lastTime = endTime;
						}
						noteCount ++;
					}
				case MIDIEventType.noteOn( number, velocity ):
					if( ! notes.exists( number ) ) {
						notes.set(
							number,
							{
								start: this.convTimeBase( n.time ),
								volume: Math.floor( ( velocity / 127 * expression / 127 ) * 100 ),
							}
						);
					}
				case MIDIEventType.programChange( _ ):
					// 無視
				case MIDIEventType.controlChange( controlNumber, value ):
					switch( controlNumber ) {
						case MIDIControlNumber.expression:
							expression = value;
						default:
							// 無視
					}
				case MIDIEventType.channelPressure( _ ):
					// 無視
				case MIDIEventType.pitchBend( _ ):
					// 無視
				case MIDIEventType.polyphonicKeyPressure( _, _ ):
					// 無視
				case MIDIEventType.systemEvent( _ ):
					// 無視
			}
		}

		root.set( "noteCount", Std.string( noteCount ) );

		return root;
	}


	/**
	 * 1つのトラックを変換（ドラムトラック）
	 * @param	channel
	 * @return
	 */
	private function convDrumTrack( trackIndex:Int, patternId:Int, channel:Array<MIDIEvent> ):Xml
	{
		var root = Xml.createElement( "track" );
		root.set( "trackIndex", Std.string( trackIndex ) );
		root.set( "deviceType", "2" );		// 0:FM 1:Analog 2:Sample

		// ノート配置
		var expression:Int = 100;
		var noteCount:Int = 0;

		for ( n in channel ) {
			switch( n.type ) {
				case MIDIEventType.noteOn( number, velocity ):
					var convNumber = switch( number ) {
						case 36: 48;	// Bass Drum 1
						case 35: 49;	// Bass Drum 2

						case 38: 50;	// Snare 1
						case 40: 51;	// Snare 2

						case 42: 55;	// Closed Hi-hat
						case 46: 58;	// Open Hi-hat

						case 51: 55;	// Ride Symbal
						case 49: 61;	// Crash Symbal
						case 57: 61;	// Crash Symbal（同じ番号に生成）

						case 41, 43, 45: 60;	// Low Tom
						case 47, 48, 50: 59;	// Hi Tom
						default: 0;
					};
					if( convNumber != 0 ) {
						var startTime = this.convTimeBase( n.time );
						var endTime = startTime + 16;
						var note = Xml.createElement( "seqNote" );
						note.set( "startTick", Std.string( startTime ) );
						note.set( "endTick", Std.string( endTime ) );
						note.set( "noteLength", Std.string( endTime - startTime ) );
						note.set( "pitch", Std.string( convNumber ) );
						note.set( "noteVolume", Std.string( Math.floor( ( velocity / 127 * expression / 127 ) * 100 ) ) );
						note.set( "selected", "false" );
						note.set( "patternId", Std.string( patternId ) );
						note.set( "noteCutoff", "100" );
						root.addChild( note );
						if ( this.lastTime < endTime ) {
							this.lastTime = endTime;
						}
						noteCount ++;
					}
				case MIDIEventType.noteOff( _, _ ):
					// 無視（NoteOnで自動生成）
				case MIDIEventType.programChange( _ ):
					// 無視
				case MIDIEventType.controlChange( controlNumber, value ):
					switch( controlNumber ) {
						case MIDIControlNumber.expression:
							expression = value;
						default:
							// 無視
					}
				case MIDIEventType.channelPressure( _ ):
					// 無視
				case MIDIEventType.pitchBend( _ ):
					// 無視
				case MIDIEventType.polyphonicKeyPressure( _, _ ):
					// 無視
				case MIDIEventType.systemEvent( _ ):
					// 無視
			}
		}

		root.set( "noteCount", Std.string( noteCount ) );

		return root;
	}

	/**
	 * ソングパタン生成
	 * @return
	 */
	private function generateSongPattern( ):Xml
	{
		var root = Xml.createElement( "songPatterns" );
		var patternId:Int = 1;

		for ( track in 0 ... this.trackCount ) {
			var pattern = Xml.createElement( "pattern" );
			pattern.set( "trackNro", Std.string( track ) );
			pattern.set( "patternId", Std.string( patternId ) );
			pattern.set( "patternColor", Std.string( track % 10 ) );
			pattern.set( "startTick", "0" );
			pattern.set( "endTick", Std.string( this.lastTime ) );
			pattern.set( "patternLength", Std.string( this.lastTime ) );
			pattern.set( "selected", "false" );
			root.addChild( pattern );

			patternId ++;
		}

		root.set( "count", Std.string( this.trackCount ) );

		return root;
	}

	/**
	 * デバイス生成
	 * @return
	 */
	private function generateDevices( ):Xml
	{
		var root = Xml.createElement( "devices" );
		var id:Int = 0;

		var channels = MIDIUtil.parseChannelsFromTrack( this.music.tracks[0] );

		for ( t in channels ) {
			if( 0 < t.length ) {
				var device = Xml.createElement( "audioDevice" );
				device.set( "deviceType", ( id == 9 ) ? "2" : "0" );	// 0:FM 1:Analog 2:Sampler
				device.set( "visible", "false" );	// window
				device.set( "xpos", "128" );		// window-x
				device.set( "ypos", "128" );		// window-y
				device.set( "childIndex", Std.string( id ) );	// たぶん ページ数

				// サウンド定義（手抜き）TODO:なんとかする
				var sound = if ( id == 9 ) {
					Xml.parse( "<sampler><masterAttack>0.003</masterAttack><masterDecay>2.006</masterDecay><masterSustain>0.69</masterSustain><masterRelease>3.735</masterRelease><filterAttackDepth>0.5</filterAttackDepth><filterAttack>0</filterAttack><filterDecay>0</filterDecay><filterSustain>0</filterSustain><filterRelease>0</filterRelease><masterTranspose>0</masterTranspose><masterAmp>99</masterAmp><overdrive>4</overdrive><modulate>0</modulate><bitrate>0</bitrate><lfoToggled>false</lfoToggled><lfoWaveForm>0</lfoWaveForm><lfoFilter>0.4</lfoFilter><lfoPitch>0</lfoPitch><lfoSpeed>0.29</lfoSpeed><lfoDelay>0</lfoDelay><chorusSize>100</chorusSize><chorusSpeed>5</chorusSpeed><chorusDryWet>0</chorusDryWet><filterType>0</filterType><cutoff>100</cutoff><resonance>0</resonance><samples count=\"23\"><cell id=\"0\"><name>Kick 1</name><url>./sounds/tightkit/sample_0.mp3</url><semitone>-3</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>48</loKey><hiKey>48</hiKey><rootKey>48</rootKey></cell><cell id=\"1\"><name>kick 2</name><url>./sounds/tightkit/sample_1.mp3</url><semitone>-2</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>49</loKey><hiKey>49</hiKey><rootKey>49</rootKey></cell><cell id=\"2\"><name>Snare 1</name><url>./sounds/tightkit/sample_2.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>50</loKey><hiKey>50</hiKey><rootKey>50</rootKey></cell><cell id=\"3\"><name>Snare 2</name><url>./sounds/tightkit/sample_3.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>51</loKey><hiKey>51</hiKey><rootKey>51</rootKey></cell><cell id=\"4\"><name>Snare 3</name><url>./sounds/tightkit/sample_4.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>52</loKey><hiKey>52</hiKey><rootKey>52</rootKey></cell><cell id=\"5\"><name>Snare 4</name><url>./sounds/tightkit/sample_5.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>53</loKey><hiKey>53</hiKey><rootKey>53</rootKey></cell><cell id=\"6\"><name>Hi-Hat Open</name><url>./sounds/tightkit/sample_6.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>112</volume><pan>-38</pan><smpStart>6.5</smpStart><smpEnd>66</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>54</loKey><hiKey>54</hiKey><rootKey>54</rootKey></cell><cell id=\"7\"><name>Hi-Hat 1</name><url>./sounds/tightkit/sample_7.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>-23</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>55</loKey><hiKey>55</hiKey><rootKey>55</rootKey></cell><cell id=\"8\"><name>Hi-Hat 2</name><url>./sounds/tightkit/sample_8.mp3</url><semitone>21</semitone><finetone>0</finetone><volume>100</volume><pan>37</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>56</loKey><hiKey>56</hiKey><rootKey>56</rootKey></cell><cell id=\"9\"><name>Hi-Hat 3</name><url>./sounds/tightkit/sample_9.mp3</url><semitone>6</semitone><finetone>0</finetone><volume>100</volume><pan>42</pan><smpStart>0</smpStart><smpEnd>9.25</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>57</loKey><hiKey>57</hiKey><rootKey>57</rootKey></cell><cell id=\"10\"><name>Hi-Hat 4</name><url>./sounds/tightkit/sample_10.mp3</url><semitone>-3</semitone><finetone>0</finetone><volume>56</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>58</loKey><hiKey>58</hiKey><rootKey>58</rootKey></cell><cell id=\"11\"><name>Tom 1</name><url>./sounds/tightkit/sample_11.mp3</url><semitone>-2</semitone><finetone>0</finetone><volume>100</volume><pan>-51</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>59</loKey><hiKey>59</hiKey><rootKey>59</rootKey></cell><cell id=\"12\"><name>Tom 2</name><url>./sounds/tightkit/sample_12.mp3</url><semitone>-2</semitone><finetone>0</finetone><volume>100</volume><pan>38</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>60</loKey><hiKey>60</hiKey><rootKey>60</rootKey></cell><cell id=\"13\"><name>Splash</name><url>./sounds/tightkit/sample_13.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>145</volume><pan>44</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>61</loKey><hiKey>61</hiKey><rootKey>61</rootKey></cell><cell id=\"14\"><name>China 1</name><url>./sounds/tightkit/sample_14.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>48</pan><smpStart>11</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>62</loKey><hiKey>62</hiKey><rootKey>62</rootKey></cell><cell id=\"15\"><name>Ride Low 1</name><url>./sounds/tightkit/sample_15.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>75</loKey><hiKey>75</hiKey><rootKey>75</rootKey></cell><cell id=\"16\"><name>China 2</name><url>./sounds/tightkit/sample_16.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>18.75</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>63</loKey><hiKey>63</hiKey><rootKey>63</rootKey></cell><cell id=\"17\"><name>Ride Low 2</name><url>./sounds/tightkit/sample_17.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>-48</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>64</loKey><hiKey>64</hiKey><rootKey>64</rootKey></cell><cell id=\"18\"><name>Ride Low 3</name><url>./sounds/tightkit/sample_18.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>-65</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>65</loKey><hiKey>65</hiKey><rootKey>65</rootKey></cell><cell id=\"19\"><name>Ride Low 4</name><url>./sounds/tightkit/sample_19.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>66</loKey><hiKey>66</hiKey><rootKey>66</rootKey></cell><cell id=\"20\"><name>Ride Low 5</name><url>./sounds/tightkit/sample_20.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>-38</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>71</loKey><hiKey>71</hiKey><rootKey>71</rootKey></cell><cell id=\"22\"><name>Slash 1</name><url>./sounds/tightkit/sample_21.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>100</volume><pan>51</pan><smpStart>0</smpStart><smpEnd>100</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>74</loKey><hiKey>74</hiKey><rootKey>74</rootKey></cell><cell id=\"21\"><name>Slash 2</name><url>./sounds/tightkit/sample_22.mp3</url><semitone>0</semitone><finetone>0</finetone><volume>60</volume><pan>0</pan><smpStart>0</smpStart><smpEnd>57.5</smpEnd><loopStart>0</loopStart><loopEnd>100</loopEnd><loopMode>off</loopMode><playMode>forward</playMode><loKey>70</loKey><hiKey>70</hiKey><rootKey>70</rootKey></cell></samples></sampler>" );
				}else {
					Xml.parse( "<sound><masterTranspose>0</masterTranspose><masterAmp>100</masterAmp><presetIndex>16</presetIndex><opAmp1>10</opAmp1><opAmp2>4</opAmp2><opAmp3>20</opAmp3><opAmp4>20</opAmp4><frq1>1</frq1><frq2>1</frq2><frq3>1</frq3><frq4>1</frq4><fine1>-0.001</fine1><fine2>-0.3</fine2><fine3>0.001</fine3><fine4>0</fine4><fmFeedBack>2.5</fmFeedBack><keybOctave>3</keybOctave><portamento>0</portamento><fmAlgorithm>5</fmAlgorithm><attack>0</attack><decay>1.64</decay><sustain>0</sustain><release>0.09</release><filterAttack>0</filterAttack><filterDecay>0</filterDecay><filterSustain>0</filterSustain><filterRelease>0</filterRelease><filterAttackDepth>0.5</filterAttackDepth><filterMode>2</filterMode><cutoff>78</cutoff><resonance>20</resonance><lfoTime>0.125</lfoTime><lfoFilter>20</lfoFilter><lfoPitch>0.002</lfoPitch><lfoActive>true</lfoActive><waveform>0</waveform><bitrate>0</bitrate><overdrive>1</overdrive><driveModul>50</driveModul><chorusLevel>100</chorusLevel><chorusSpeed>5</chorusSpeed><chorusMix>75</chorusMix><aOp1>0</aOp1><aOp2>0</aOp2><aOp3>0</aOp3><aOp4>0</aOp4><dOp1>0.9</dOp1><dOp2>0.05</dOp2><dOp3>0</dOp3><dOp4>0.2</dOp4><sOp1>0</sOp1><sOp2>0</sOp2><sOp3>0</sOp3><sOp4>75</sOp4></sound>" );
				};
				device.addChild( sound.firstChild( ) );

				root.addChild( device );
			}
			id ++;
		}

		root.set( "count", Std.string( id ) );

		return root;
	}
}


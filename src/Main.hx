package;

import haxe.crypto.Crc32;
import haxe.io.Output;
import haxe.zip.Entry;
import haxe.zip.Writer;
import haxe.zip.Tools;
import haxe.io.Bytes;
import neko.Lib;
import sys.io.File;

/**
 * フロントエンド
 * @author あるる（きのもと 結衣）
 */
class Main 
{
	/**
	 * エントリポイント
	 */
	static function main() 
	{
		var src = "cr_map.mid";
		var dest = "test.song";

		var args = Sys.args( );
		if ( args.length == 0 ) {
			Lib.println( "usage: m2sauna src [dest]" );
			return;
		}

		src = args[0];
		if( 2 <= args.length ) {
			dest = args[1];
		}

		var smfReader = new format.mid.Reader( File.read( src, true ) );
		var xml = SMF2Sauna.conv( smfReader.read( ) ).toString( );
		File.saveContent( "test.xml", xml );
		var files = new List<Entry>( );
		files.add( {
			compressed: false,
			crc32: Crc32.make( Bytes.ofString( xml ) ),
			data: Bytes.ofString( xml ),
			dataSize: xml.length,
			fileName: "songdata.xml",
			fileSize: xml.length,
			fileTime: Date.now( ),
		});
		for ( t in files ) {
			Tools.compress( t, 2 );
		}
		var zipWriter = new Writer( File.write( dest, true ) );
		zipWriter.write( files );
	}
}

package ;

import haxe.Utf8;
import noriko.str.JCode;

#if neko
import neko.Lib;
#elseif cpp
import cpp.Lib;
#elseif java
import java.lang.System;
#elseif cs
import cs.system.Console;
#else
#error sorry.
#end

/**
 * 共通処理
 * @author あるる（きのもと 結衣）
 */
class Common
{
	/**
	 * （特定環境でも表示出来るように）文字コード変換付き文字表示
	 * @param	d	文字列
	 */
	static public function println( d:Dynamic ):Void
	{
		#if ( neko || cpp )
			if( Sys.systemName() == "Windows" ){
				var s = Std.string( d );

				Lib.print( JCode.toSJIS( s ) );
				Lib.println( "" );
			}else {
				Lib.println( d );
			}
		#elseif cs
			Console.WriteLine( Std.string( d ) );
		#elseif java
			System.out.println( Std.string( d ) );
		#end
	}

	/**
	 * （特定環境でも表示出来るように）文字コード変換付き文字表示
	 * @param	d	文字列
	 */
	static public function print( d:Dynamic ):Void
	{
		#if ( neko || cpp )
			if( Sys.systemName() == "Windows" ){
				var s = Std.string( d );

				Lib.print( JCode.toSJIS( s ) );
			}else {
				Lib.print( d );
			}
		#elseif cs
			Console.Write( Std.string( d ) );
		#elseif java
			System.out.print( Std.string( d ) );
		#end
	}

}